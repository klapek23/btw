import angular from 'angular';

import ProductCarouselController from './product-carousel-controller.js';
import ProductCarouselStyles from './product-carousel-styles.scss';

export default class ProductCarouselComponent {
    constructor($timeout, $window, $q) {
        this.restrict = 'E';
        this.controller = ProductCarouselController;
        this.controllerAs = '$ctrl';
        this.bindToController = true;
        this.scope = {
            items: '<',
            closeText: '@',
            openText: '@'
        };
        this.templateUrl = '/wp-content/themes/btw/app/products-carousel/product-carousel-view.html';
        this.link = function($scope, $elem, $attrs, $ctrl) {
            var visible,
                itemWidth = 0,
                slider,
                items;

            let wWidth = angular.element($window).width();
            switch(true) {
                case (wWidth < 480):
                    visible = $ctrl.visible = 1;
                    break;

                case (wWidth < 992):
                    visible = $ctrl.visible = 2;
                    break;

                default:
                    visible = $ctrl.visible = 4;
                    break;
            }

            var promisesArray = [];
            angular.forEach($ctrl.items, function(item, i) {
                promisesArray.push(imgOnLoad(item.image.url));
            });


            $timeout(function() {
                slider = angular.element('#product-carousel');
                items = slider.children();

                $q.all(promisesArray).then(function() {
                    calculateItemWidth();
                    calculateWidth();
                    calculateHeight();

                    $elem.addClass('calculated');
                });
            });

            angular.element($window).bind('resize', function() {
                visible = $ctrl.visible = (angular.element($window).width() < 768 ? 2 : 4);

                let wWidth = angular.element($window).width();
                switch(true) {
                    case (wWidth < 480):
                        visible = $ctrl.visible = 1;
                        break;

                    case (wWidth < 992):
                        visible = $ctrl.visible = 2;
                        break;

                    default:
                        visible = $ctrl.visible = 4;
                        break;

                    $scope.$apply();
                }

                calculateItemWidth();
                calculateHeight();
                calculateWidth();
                calculatePosition();
            });

            function imgOnLoad(url) {
                return new $q(function(resolve, reject) {
                    var img = new Image();
                    img.onload = function() {
                        resolve();
                    };

                    img.onerror = function(){
                        reject(url)
                    };

                    img.src = url;
                });
            }

            function calculateHeight() {
                var maxHeight = 0;
                angular.forEach(items, function(item, i) {
                    var imgHeight = angular.element(item).height();
                    if(imgHeight > maxHeight) {
                        maxHeight = imgHeight;
                    }
                });

                slider.height(maxHeight);
            }

            function calculateWidth() {
                slider.width(($ctrl.items.length + 1) * itemWidth);
            }

            function calculateItemWidth() {
                $ctrl.itemWidth = itemWidth = slider.parent().width() / visible;
                angular.forEach(items, function(item, i) {
                    angular.element(item).width(itemWidth);
                });
            }

            function calculatePosition() {
                $ctrl.sliderPosition = $ctrl.activeItem * itemWidth;
                $scope.$apply();
            }

            function refreshSlider() {
                calculateItemWidth();
                calculateHeight();
                calculateWidth();
                calculatePosition();
            }
        }
    }
}

