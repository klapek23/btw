import angular from 'angular';

import ProductCarouselButtonController from './product-carousel-button-controller.js';

export default class ProductCarouselComponent {
    constructor($timeout, $window, $q) {
        this.restrict = 'E';
        this.controller = ProductCarouselButtonController;
        this.controllerAs = '$ctrl';
        this.bindToController = true;
        this.scope = {
            closeText: '@',
            openText: '@',
            sliderOpened: '=',
            toggleSlider: '&'
        };
        this.templateUrl = '/wp-content/themes/btw/app/products-carousel/product-carousel-button-view.html';
        this.link = function($scope, $elem, $attrs, $ctrl) {
        }
    }
}

