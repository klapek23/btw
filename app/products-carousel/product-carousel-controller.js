export default class ProductCarouselCtrl{
  constructor($scope, $interval, productCarouselService){
    this.$scope = $scope;
    this.$interval = $interval;
    this.carouselService = productCarouselService;
    this.sliderOpened = false;
    this.activeItem = 0;
    this.itemWidth = 0;
    this.visible = 0;
    this.sliderPosition = 0;
    this.slideTimeout = 6000;

    this.startAnimationInterval();
  }

  startAnimationInterval() {
      this.animationInterval = this.$interval(() => {
          let newItem = (this.activeItem < this.items.length - this.visible ? this.activeItem + 1 : 0);
          this.changeSlide(false, newItem);
      }, this.slideTimeout);
  }

  changeSlide(userClick, index, event) {
      if(index - 1 < this.items.length - this.visible && index >= 0) {
          this.activeItem = index;
          this.sliderPosition = index * this.itemWidth;
      } else {
          this.activeItem = 0;
          this.sliderPosition = 0;
      }

      if(userClick) {
          this.$interval.cancel(this.animationInterval);
          this.startAnimationInterval();
      }

      if(event) {
          event.preventDefault();
          event.stopPropagation();
      }
  }

  stopScroll(event) {
      event.preventDefault();
      event.stopPropagation();
  }

  toggleSlider() {
      this.sliderOpened = !this.sliderOpened;
  }
}

ProductCarouselCtrl.$inject = ["$scope", "$interval", "productCarouselService"];
