export default class BoxMultipleCtrl{
  constructor($timeout, $interval){
    this.$timeout = $timeout;
    this.$interval = $interval;

    angular.element(document.getElementsByTagName('body')).css('height', 'auto');

    this.animateElementIn = function($el) {
      var $leftSide = $el.find('article'),
          $rightSide = $leftSide.next(),
          wWidht = window.innerWidth;

      $el.removeClass('not-visible');
      $el.addClass('animated active fadeIn');

      $leftSide.removeClass('not-visible');
      $rightSide.removeClass('not-visible');

      if(wWidht > 767) {
        if ($el.hasClass('even')) {
          $leftSide.addClass('animated fadeInLeft');
          $rightSide.addClass('animated fadeInRight');
        } else {
          $leftSide.addClass('animated fadeInRight');
          $rightSide.addClass('animated fadeInLeft');
        }
      } else {
        $leftSide.addClass('animated fadeIn');
        $rightSide.addClass('animated fadeIn');
      }
    };
  }
}

BoxMultipleCtrl.$inject = ["$timeout", "$interval"];
