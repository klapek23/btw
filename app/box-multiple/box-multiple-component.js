import angular from 'angular';

import BoxMultipleController from './box-multiple-controller.js';
import BoxMultipleStyles from './box-multiple-styles.scss';

export default class BoxMultipleComponent {
    constructor() {
        this.restrict = 'E';
        this.controller = BoxMultipleController;
        this.controllerAs = '$ctrl';
        this.bindToController = true;
        this.scope = {
            items: '<'
        };
        this.template = `<div class="item not-visible" when-visible="$ctrl.animateElementIn" ng-class="(key % 2 === 0 ? 'even' : 'odd')" ng-repeat="(key, item) in $ctrl.items">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="center">
                            <article class="not-visible">
                                <h3 ng-bind-html="item.title"></h3>
                                <p ng-bind-html="item.content"></p>
                            </article>
                            <div class="image not-visible">
                                <img ng-src="{{::item.image}}" alt="{{::item.title}}" class="img-responsive">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`;
    }
}
