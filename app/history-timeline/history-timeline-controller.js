export default class HistoryTimelineCtrl{
  constructor($timeout, $interval, historyTimelineService){
    this.$timeout = $timeout;
    this.$interval = $interval;
    this.historyTimelineService = historyTimelineService;

    angular.element(document.getElementsByTagName('body')).css('height', 'auto');

    this.animateElementIn = function($el) {
      var $leftSide = $el.children().eq(0),
          $center = $el.children().eq(1),
          $rightSide = $el.children().eq(2),
          wWidht = window.innerWidth;

      $el.removeClass('not-visible');
      $el.addClass('animated active fadeIn');

      $leftSide.removeClass('not-visible');
      $rightSide.removeClass('not-visible');

      if(wWidht > 767) {
        if ($el.hasClass('even')) {
          $leftSide.addClass('animated fadeInLeft');
          $rightSide.addClass('animated fadeInRight');
        } else {
          $leftSide.addClass('animated fadeInRight');
          $rightSide.addClass('animated fadeInLeft');
        }
      } else {
        $leftSide.addClass('animated fadeIn');
        $rightSide.addClass('animated fadeIn');
      }

      $center.children().eq(1).removeClass('not-visible').addClass('animated fadeIn zoomIn');
    };
  }
}

HistoryTimelineCtrl.$inject = ["$timeout", "$interval", "historyTimelineService"];
