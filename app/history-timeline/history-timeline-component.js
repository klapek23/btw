import angular from 'angular';

import HistoryTimelineController from './history-timeline-controller.js';
import HistoryTimelineStyles from './history-timeline-styles.scss';

export default class HistoryTimelineComponent {
    constructor() {
        this.restrict = 'E';
        this.controller = HistoryTimelineController;
        this.controllerAs = '$ctrl';
        this.bindToController = true;
        this.scope = {
            items: '<'
        };
        this.template = `<div class="item not-visible" when-visible="$ctrl.animateElementIn" ng-class="(key % 2 === 0 ? 'even' : 'odd')" ng-repeat="(key, item) in $ctrl.items">
            <div class="image not-visible">
                <img ng-src="{{::item.image}}" alt="{{::item.name}}" class="img-responsive">
            </div>
            <div class="date">
                <div class="axis"></div>
                <span class="not-visible">{{::item.year}}</span>
            </div>
            <article class="not-visible"">
                <h3 ng-bind-html="item.name"></h3>
                <p ng-bind-html="item.description"></p>
            </article>
        </div>`;
    }
}
