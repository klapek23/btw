module.exports = {
    "files": [
        "../icons/behance.svg",
        "../icons/behance-transparent.svg",
        "../icons/dribbble.svg",
        "../icons/dribbble-transparent.svg",
        "../icons/linkedin.svg",
        "../icons/linkedin-transparent.svg",
        "../icons/arrow-left.svg",
        "../icons/arrow-bold-left.svg",
        "../icons/empty-triangle-left.svg",
        "../icons/close.svg"
    ],
    "fontName": "icons",
    "classPrefix": "icon-",
    "baseClass": "icon",
    "fixedWidth": true,
    "types": ["eot", "woff", "ttf", "svg"] // this is the default
}