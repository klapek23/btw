import angular from 'angular';

export default class DialogDirective {
    constructor($timeout, $window, ngDialog) {
        this.restrict = 'A';
        this.controller = function() {};
        this.controllerAs = '$ctrl';
        this.bindToController = true;
        this.scope = {
            dialogData: '='
        };
        this.link = function ($scope, $elem, $attrs, $ctrl) {
            $elem.on('click', function() {
                ngDialog.open({
                    template: '/wp-content/themes/btw/app/shared/directives/dialogView.html',
                    className: 'btw-dialog',
                    data: $ctrl.dialogData,
                    controller: function($scope) {

                    }
                })
            });
        };
    }
}