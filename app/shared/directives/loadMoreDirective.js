import angular from 'angular';

export default class LoadMoreDirective {
    constructor($timeout, $window) {
        this.restrict = 'A';
        this.controller = function() {};
        this.controllerAs = '$ctrl';
        this.bindToController = true;
        this.scope = {
            itemsCount: '='
        };
        this.link = function ($scope, $elem, $attrs, $ctrl) {
            var visible = $elem.parent().parent().parent().prev().find('.item.visible').length;

            if($ctrl.itemsCount <= visible) {
                $elem.addClass('hidden');
            }

            $elem.on('click', function() {
                var $items = $elem.parent().parent().parent().prev().find('.item').not('.visible'),
                    index = 0;
                angular.forEach($items, function(item, i) {
                    console.log(angular.element(item));
                    if(i < 3) {
                        angular.element(item).addClass('visible');
                        visible++;
                    }
                    index++;
                });

                if($ctrl.itemsCount <= visible) {
                    $elem.addClass('hidden');
                }
            });
        };
    }
}