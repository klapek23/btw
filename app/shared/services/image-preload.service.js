export default class Preloader {
  constructor($q) {
    this.$q = $q;
    this.images = [];
    this.promises = [];
  }

  init(images, callback) {
    this.getImages(images);
    this.addImages();
    return this.$q.all(this.promises);
  }

  getImages(images) {
    angular.forEach(images, (img) => {
      this.images.push(img);
    });
  }

  addImages() {
    angular.forEach(this.images, (img) => {
      var deffered = this.$q.defer(),
          image = new Image;

      image.src = img.url;

      if (image.complete) {
        deffered.resolve();
      } else {
        image.addEventListener('load', () => {
          deffered.resolve();
        });

        image.addEventListener('error', () => {
          deffered.reject();
          throw new Error('Image ' + image.src + ' not loaded!');
        });
      }

      this.promises.push(deffered.promise);
    });
  }
}