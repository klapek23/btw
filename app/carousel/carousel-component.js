import angular from 'angular';

import CarouselController from './carousel-controller.js';
import CarouselStyles from './carousel-styles.scss';

export default class CarouselComponent {
    constructor($timeout, $window, $q) {
        this.restrict = 'E';
        this.controller = CarouselController;
        this.controllerAs = '$ctrl';
        this.bindToController = true;
        this.scope = {
            items: '<'
        };
        this.templateUrl = '/wp-content/themes/btw/app/carousel/carousel-view.html';
        this.link = function($scope, $elem, $attrs, $ctrl) {
            var visible = $ctrl.visible = (angular.element($window).width() < 768 ? 1 : 2),
                itemWidth = 0,
                slider,
                items;

            var promisesArray = [];
            angular.forEach($ctrl.items, function(item, i) {
                promisesArray.push(imgOnLoad(item.image.url));
            });


            $timeout(function() {
                slider = angular.element($elem.children()[0]);
                items = slider.children();

                $q.all(promisesArray).then(function() {
                    calculateItemWidth();
                    calculateWidth();
                    calculateHeight();

                    $elem.addClass('calculated');
                });
            });

            angular.element($window).bind('resize', function() {
                visible = $ctrl.visible = (angular.element($window).width() < 768 ? 1 : 2),

                calculateItemWidth();
                calculateHeight();
                calculateWidth();
                calculatePosition();
            });

            function imgOnLoad(url) {
                return new $q(function(resolve, reject) {
                    var img = new Image();
                    img.onload = function() {
                        resolve();
                    };

                    img.onerror = function(){
                        reject(url)
                    };

                    img.src = url;
                });
            }

            function calculateHeight() {
                var maxHeight = 0;
                angular.forEach(items, function(item, i) {
                    var imgHeight = angular.element(item).height();
                    if(imgHeight > maxHeight) {
                        maxHeight = imgHeight;
                    }
                });

                slider.height(maxHeight);
            }

            function calculateWidth() {
                slider.width(($ctrl.items.length + 1) * itemWidth);
            }

            function calculateItemWidth() {
                $ctrl.itemWidth = itemWidth = $elem.width() / visible;
                angular.forEach(items, function(item, i) {
                    angular.element(item).width(itemWidth);
                });
            }

            function calculatePosition() {
                $ctrl.sliderPosition = $ctrl.activeItem * itemWidth;
                $scope.$apply();
            }
        }
    }
}

