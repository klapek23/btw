import angular from 'angular';

import MenuController from './menu-controller.js';
import MenuStyles from './menu-styles.scss';

export default class MenuDirective {
    constructor() {
        this.restrict = 'A';
        this.controller = MenuController;
        this.controllerAs = '$ctrl';
        this.bindToController = true;
    }
}