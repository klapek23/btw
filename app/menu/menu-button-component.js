import angular from 'angular';

import MenuButtonController from './menu-button-controller.js';
import MenuButtonStyles from './menu-button-styles.scss';

export default class MenuButtonComponent {
    constructor() {
        this.restrict = 'E';
        this.controller = MenuButtonController;
        this.controllerAs = '$ctrl';
        this.bindToController = true;
        this.scope = {};
        this.template = `<button type="button" class="hamburger menu-hamburger hamburger-icon" id="toggle-menu-button"
                                ng-click="$ctrl.onClick($event)" ng-class="{toggled: $ctrl.menuService.isOpened}">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>`
    }
}