export default class MenuController {
  constructor(menuService) {
      this.menuService = menuService;
  }
}

MenuController.$inject = ["menuService"];
