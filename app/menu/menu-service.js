export default class MenuService {
    constructor() {
        this.isOpened = false;
    }

    toggleMenu() {
        this.isOpened = !this.isOpened;
    }


}

MenuService.$inject = [];