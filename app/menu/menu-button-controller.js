export default class MenuButtonController {
  constructor(menuService){
    this.menuService = menuService;
  }

  onClick($event) {
    this.menuService.toggleMenu();
    var body = document.getElementsByTagName('body'),
        html = document.getElementsByTagName('html');
    if(this.menuService.isOpened) {
        angular.element(body).css('overflow', 'hidden');
        angular.element(html).css('overflow', 'hidden');
    } else {
        angular.element(body).css('overflow', 'auto');
        angular.element(html).css('overflow', 'auto');
    }
  }
}

MenuButtonController.$inject = ["menuService"];
