export default class LoaderService {
    constructor() {
        this.isActive = false;
    }

    show() {
        this.isActive = true;
    }

    hide() {
        this.isActive = false;
    }

    _calculateHeight() {

    }
}
