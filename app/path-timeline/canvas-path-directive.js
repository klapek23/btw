import angular from 'angular';

export default class CanvasPath {
    constructor($timeout, $window, preloader) {
        this.restrict = 'A';
        this.scope = {
            images: '='
        };
        this.link = function ($scope, $elem, $attrs, $ctrl) {
            var globalContext;

            $timeout(function() {
                preloader.init($scope.images).then(() => {
                    $elem[0].width = $elem.parent().width();
                    $elem[0].height = $elem.parent().height();

                    var wWidth = angular.element($window).width(),
                      items = $elem.parent().find('.item-container');

                    if (wWidth > 991) {
                        globalContext = draw(items);
                    }
                }, (err) => {
                    console.log(err);
                });
            });

            angular.element($window).bind('resize', function() {
                var wWidth = angular.element($window).width(),
                    items = $elem.parent().find('.item-container');

                if(wWidth > 991) {
                    globalContext.clearRect(0, 0, $elem.width(), $elem.height());
                    draw(items);
                } else {
                    globalContext.clearRect(0, 0, $elem.width(), $elem.height());
                }
            });

            function draw(items) {
                var lastX, lastY,
                    ctx = $elem[0].getContext('2d');

                ctx.beginPath();

                angular.forEach(items, function(item, i) {
                    var $item = angular.element(item),
                        $itemButton = $item.find('.item-button:not(.rwd)'),
                        itemButtonRect = $itemButton[0].getBoundingClientRect(),
                        itemButtonPosition = {
                            left: $itemButton.offset().left,
                            top: $itemButton.offset().top
                        };

                    itemButtonPosition.middle = {
                        x: (itemButtonPosition.left + (itemButtonRect.width / 2)) - $elem.offset().left,
                        y: (itemButtonPosition.top + (itemButtonRect.height / 2)) - $elem.offset().top,
                    };

                    ctx.strokeStyle = "#137a8c";
                    ctx.lineWidth = 3;

                    if($item.index() == 1) {
                        ctx.moveTo(itemButtonPosition.middle.x, itemButtonPosition.middle.y);
                    } else if($item.index() == 2) {
                        var cp = {
                            x: itemButtonPosition.middle.x,
                            y: (itemButtonPosition.middle.y - ((itemButtonPosition.middle.y - lastY) / 2)) - 40
                        };

                        ctx.quadraticCurveTo(cp.x, cp.y, itemButtonPosition.middle.x, itemButtonPosition.middle.y);
                    } else if($item.index() % 2 == 1) {
                        var cp = {
                                x: lastX,
                                y: (itemButtonPosition.middle.y - ((itemButtonPosition.middle.y - lastY) / 2)) - 40
                            },
                            mp = {
                                x: lastX - ((lastX - itemButtonPosition.middle.x) / 2),
                                y: lastY + ((itemButtonPosition.middle.y - lastY) / 2),
                            };

                        ctx.quadraticCurveTo(cp.x, cp.y, mp.x, mp.y);
                        ctx.quadraticCurveTo(itemButtonPosition.middle.x, mp.y + 40, itemButtonPosition.middle.x, itemButtonPosition.middle.y);
                    } else if($item.index() % 2 == 0) {
                        var cp = {
                                x: lastX,
                                y: (itemButtonPosition.middle.y - ((itemButtonPosition.middle.y - lastY) / 2)) - 40
                            },
                            mp = {
                                x: lastX - ((lastX - itemButtonPosition.middle.x) / 2),
                                y: lastY + ((itemButtonPosition.middle.y - lastY) / 2),
                            };

                        ctx.quadraticCurveTo(cp.x, cp.y, mp.x, mp.y);
                        ctx.quadraticCurveTo(itemButtonPosition.middle.x, mp.y + 40, itemButtonPosition.middle.x, itemButtonPosition.middle.y);
                    }

                    ctx.stroke();

                    lastX = itemButtonPosition.middle.x;
                    lastY = itemButtonPosition.middle.y;
                });

                return ctx;
            }
        };
    }
}