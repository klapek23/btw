export default class PathTimelineCtrl{
  constructor($scope, $timeout, $interval, pathTimelineService){
    this.$scope = $scope;
    this.$timeout = $timeout;
    this.$interval = $interval;
    this.pathTimelineService = pathTimelineService;
    this.itemsImages = [];

    angular.element(document.getElementsByTagName('body')).css('height', 'auto');

    angular.forEach(this.items, (item, i) => {
      this.itemsImages.push(item.image);
    });

    this.animateElementIn = function($el) {
      var wWidht = window.innerWidth;

      if($el.hasClass('not-visible')) {
        this.$scope.$emit('item-show', {itemEl: $el});
      }

      $el.removeClass('not-visible');
      $el.addClass('animated active fadeIn');
    }.bind(this);
  }
}

PathTimelineCtrl.$inject = ["$scope", "$timeout", "$interval", "pathTimelineService"];
