import angular from 'angular';

import PathTimelineController from './path-timeline-controller.js';
import PathTimelineStyles from './path-timeline-styles.scss';

export default class PathTimelineComponent {
    constructor() {
        this.restrict = 'E';
        this.controller = PathTimelineController;
        this.controllerAs = '$ctrl';
        this.bindToController = true;
        this.scope = {
            items: '<'
        };
        this.templateUrl = '/wp-content/themes/btw/app/path-timeline/path-timeline-view.html';
    }
}
