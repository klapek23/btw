import angular from 'angular';

import PopupSliderController from './popup-slider-controller.js';
import PopupSliderStyles from './popup-slider-styles.scss';

export default class PopupSliderComponent {
    constructor($timeout, $window) {
        this.restrict = 'E';
        this.controller = PopupSliderController;
        this.controllerAs = '$ctrl';
        this.bindToController = true;
        this.scope = {
            slides: '<'
        };
        this.templateUrl = '/wp-content/themes/btw/app/popup-slider/popup-slider-view.html';
        this.link = function($scope, $elem, $attrs, $ctrl) {
            $timeout(function() {
                $elem.addClass('calculated');
                //angular.element($elem.children()[0]).height(calculateHeight());
            }, 2000);

            angular.element($window).bind('resize', function() {
                //angular.element($elem.children()[0]).height(calculateHeight());
            });

            function calculateHeight() {
                var maxHeight = 0;
                angular.forEach($elem.find('.slide'), function(slide, i) {
                    var slideHeight = angular.element(slide).height();
                    if(slideHeight > maxHeight) {
                        maxHeight = slideHeight;
                    }
                });

                return maxHeight;
            }
        }
    }
}

