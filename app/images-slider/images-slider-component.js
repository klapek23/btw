import angular from 'angular';

import ImagesSliderController from './images-slider-controller.js';
import ImagesSliderStyles from './images-slider-styles.scss';

export default class ImagesSliderComponent {
    constructor($timeout, $window) {
        this.restrict = 'E';
        this.controller = ImagesSliderController;
        this.controllerAs = '$ctrl';
        this.bindToController = true;
        this.scope = {
            images: '<'
        };
        this.templateUrl = '/wp-content/themes/btw/app/images-slider/images-slider-view.html';
        this.link = function($scope, $elem, $attrs, $ctrl) {
            $timeout(function() {
                $elem.addClass('calculated');
                angular.element($elem.children()[0]).height(calculateHeight());
            }, 1000);

            angular.element($window).bind('resize', function() {
                angular.element($elem.children()[0]).height(calculateHeight());
            });

            function calculateHeight() {
                var maxHeight = 0;
                angular.forEach($elem.find('.slide-image'), function(img, i) {
                    var imgHeight = angular.element(img).height();
                    if(imgHeight > maxHeight) {
                        maxHeight = imgHeight;
                    }
                });

                return maxHeight;
            }
        }
    }
}

