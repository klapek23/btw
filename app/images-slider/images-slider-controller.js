export default class ImagesSliderCtrl{
  constructor($interval, imagesSliderService){
    this.$interval = $interval;
    this.imagesSliderService = imagesSliderService;
    this.activeItem = 0;
    this.slideTimeout = 6000;

    this.startAnimationInterval();
  }

  startAnimationInterval() {
      this.animationInterval = this.$interval(() => {
          let newItem = (this.activeItem + 1 < this.images.length ? this.activeItem + 1 : 0);
          this.changeSlide(false, newItem);
      }, this.slideTimeout);
  }

  changeSlide(userClick, index) {
      if(index < this.images.length && index >= 0) {
          this.activeItem = index;
      } else {
          this.activeItem = 0;
      }

      if(userClick) {
          this.$interval.cancel(this.animationInterval);
          this.startAnimationInterval();
      }
  }
}

ImagesSliderCtrl.$inject = ["$interval", "imagesSliderService"];
