import angular from 'angular';

import CookiesController from './cookies-controller.js';
import CookiesStyles from './cookies-styles.scss';

export default class CookiesComponent {
    constructor() {
        this.restrict = 'E';
        this.controller = CookiesController;
        this.controllerAs = '$ctrl';
        this.bindToController = true;
        this.scope = {};
        this.transclude = true;
        this.template = `<div class="cookies-box" ng-if="$ctrl.showCookies">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div ng-transclude></div>
                        <button type="button" class="close-cookies" ng-click="$ctrl.onCloseClick()"></button>
                    </div>
                </div>
            </div>
        </div>`;
    }
}
