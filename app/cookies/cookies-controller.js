export default class CookiesCtrl{
  constructor($cookies){
    this.$cookies = $cookies;

    this.showCookies = (this.getCookie() ? false : true);
  }

  onCloseClick() {
    this.setCookie();
    this.showCookies = false;
  }

  setCookie() {
    this.$cookies.put('accept_cookies', 1);
  }

  getCookie() {
    return this.$cookies.get('accept_cookies');
  }
}

CookiesCtrl.$inject = ["$cookies"];
