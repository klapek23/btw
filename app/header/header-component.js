import angular from 'angular';

import HeaderController from './header-controller.js';
import HeaderStyles from './header-styles.scss';

export default class HeaderDirective {
    constructor() {
        this.restrict = 'A';
        this.controller = HeaderController;
        this.controllerAs = '$ctrl';
        this.bindToController = true;
        this.scope = {}
    }
}