import angular from 'angular';

import AppController from './app-controller.js';

export default class AppDirective {
    constructor() {
        this.restrict = 'A';
        this.controller = AppController;
        this.controllerAs = '$ctrl';
        this.bindToController = true;
        this.scope = {}
    }
}