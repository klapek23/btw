export default class AppController{
  constructor(appService){
    this.appService = appService;
  }
}

AppController.$inject = ["appService"];
