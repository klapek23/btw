export default class SectionSliderFullWidthCtrl{
  constructor($interval, sectionSliderFullWidthService){
    this.$interval = $interval;
    this.sectionSliderFullWidthService = sectionSliderFullWidthService;
    this.pagination = (this.items.length > 1 ? true : false);
    this.activeItem = 0;
    this.slideTimeout = 6000;

    this.startAnimationInterval();
  }

  startAnimationInterval() {
      this.animationInterval = this.$interval(() => {
          let newItem = (this.activeItem + 1 < this.items.length ? this.activeItem + 1 : 0);
          this.changeSlide(false, newItem);
      }, this.slideTimeout);
  }

  changeSlide(userClick, index) {
      this.activeItem = index;

      if(userClick) {
          this.$interval.cancel(this.animationInterval);
          this.startAnimationInterval();
      }
  }
}

SectionSliderFullWidthCtrl.$inject = ["$interval", "sectionSliderFullWidthService"];
