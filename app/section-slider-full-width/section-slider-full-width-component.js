import angular from 'angular';

import SectionSliderFullWidthController from './section-slider-full-width-controller.js';
import SectionSliderFullWidthStyles from './section-slider-full-width-styles.scss';

export default class SectionFullWidthSliderComponent {
    constructor() {
        this.restrict = 'E';
        this.controller = SectionSliderFullWidthController;
        this.controllerAs = '$ctrl';
        this.bindToController = true;
        this.scope = {
            items: '<'
        };
        this.templateUrl = '/wp-content/themes/btw/app/section-slider-full-width/section-slider-full-width-view.html';
    }
}
