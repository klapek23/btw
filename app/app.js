import angular from 'angular';

import angularAnimate from 'angular-animate';
import uirouter from 'angular-ui-router';

require("./shared/fonts/icon.font.js");

require('angular-scroll');
require('../bower_components/angular-scroll-animate/dist/angular-scroll-animate');
require('../bower_components/angular-sanitize/angular-sanitize');
require('../bower_components/angular-cookies/angular-cookies');
require('../bower_components/angular-touch/angular-touch');
require('../bower_components/ng-dialog/js/ngDialog.min');
require('../bower_components/ng-dialog/css/ngDialog.min.css');
require('../bower_components/animate.css/animate.css');

//import bootstrap css components
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

require("./shared/fonts/icon.font.js");

//import main scss file
import './shared/sass/app.scss';

//modernizr fixes
require("!modernizr!./.modernizrrc");
Modernizr.addTest('flexboxtweener', Modernizr.testAllProps('flexAlign', 'end', true));

import AppDirective from './app/app-component.js';
import AppService from './app/app-service.js';
import PreloaderService from './shared/services/image-preload.service';

import MenuService from './menu/menu-service.js';
import MenuDirective from './menu/menu-directive.js';
import MenuButtonComponent from './menu/menu-button-component.js';

import HeaderDirective from './header/header-component.js';
import HeaderService from './header/header-service.js';

import SectionSliderFullWidthComponent from './section-slider-full-width/section-slider-full-width-component.js';
import SectionSliderFullWidthService from './section-slider-full-width/section-slider-full-width-service.js';

import ImagesSliderComponent from './images-slider/images-slider-component.js';
import ImagesSliderService from './images-slider/images-slider-service.js';

import CarouselComponent from './carousel/carousel-component.js';
import CarouselService from './carousel/carousel-service.js';

import PopupSliderComponent from './popup-slider/popup-slider-component';
import PopupSliderService from './popup-slider/popup-slider-service.js';

import ProductCarouselComponent from './products-carousel/product-carousel-component.js';
import ProductCarouselButtonComponent from './products-carousel/product-carousel-button-component.js';
import ProductCarouselService from './products-carousel/product-carousel-service.js';

import HistoryTimelineComponent from './history-timeline/history-timeline-component.js';
import HistoryTimelineService from './history-timeline/history-timeline-service.js';

import PathTimelineComponent from './path-timeline/path-timeline-component.js';
import PathTimelineService from './path-timeline/path-timeline-service.js';
import CanvasPathDirective from './path-timeline/canvas-path-directive';

import BoxMultipleComponent from './box-multiple/box-multiple-component.js';
import CookiesComponent from './cookies/cookies-component.js';

import DialogDirective from './shared/directives/dialogDirective';
import LoadMoreDirective from './shared/directives/loadMoreDirective';

export default angular.module("app", [angularAnimate, uirouter, 'ngTouch', 'ngSanitize', 'ngCookies', 'ngDialog', 'duScroll', 'angular-scroll-animate'])
    .service('appService', AppService)
    .service('preloader', ['$q', ($q) => new PreloaderService($q)])
    .service('headerService', HeaderService)
    .service('menuService', MenuService)
    .service('imagesSliderService', ImagesSliderService)
    .service('popupSliderService', PopupSliderService)
    .service('carouselService', CarouselService)
    .service('productCarouselService', ProductCarouselService)
    .service('sectionSliderFullWidthService', SectionSliderFullWidthService)
    .service('historyTimelineService', HistoryTimelineService)
    .service('pathTimelineService', PathTimelineService)
    .directive('appDirective', () => new AppDirective())
    .directive('menuDirective', () => new MenuDirective())
    .directive('dialogDirective', ($timeout, $window, ngDialog) => new DialogDirective($timeout, $window, ngDialog))
    .directive('loadMoreDirective', ($timeout, $window) => new LoadMoreDirective($timeout, $window))
    .directive('menuButtonComponent', () => new MenuButtonComponent())
    .directive('sectionSliderFullWidthComponent', () => new SectionSliderFullWidthComponent())
    .directive('imagesSliderComponent', ($timeout, $window) => new ImagesSliderComponent($timeout, $window))
    .directive('popupSliderComponent', ($timeout, $window) => new PopupSliderComponent($timeout, $window))
    .directive('carouselComponent', ($timeout, $window, $q) => new CarouselComponent($timeout, $window, $q))
    .directive('productCarouselComponent', ($timeout, $window, $q) => new ProductCarouselComponent($timeout, $window, $q))
    .directive('productCarouselButtonComponent', ($timeout, $window, $q) => new ProductCarouselButtonComponent($timeout, $window, $q))
    .directive('historyTimelineComponent', () => new HistoryTimelineComponent())
    .directive('pathTimelineComponent', () => new PathTimelineComponent())
    .directive('canvasPathDirective', ($timeout, $window, preloader) => new CanvasPathDirective($timeout, $window, preloader))
    .directive('boxMultipleComponent', () => new BoxMultipleComponent())
    .directive('cookiesComponent', () => new CookiesComponent())