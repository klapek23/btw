<!DOCTYPE html>
<!--[if lt IE 7]><html <?php language_attributes(); ?>  class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html <?php language_attributes(); ?>  class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html <?php language_attributes(); ?>  class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--><html <?php language_attributes(); ?>  class="no-js"> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?php echo get_bloginfo('name'); ?> | <?php the_title(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index,follow">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri() . '/img/favicon.ico'; ?>">

    <?php
    wp_head();

    $mainMenu = wp_get_nav_menu_items('main-menu');
    $socialMenu = wp_get_nav_menu_items('social-menu');

    $gaCode = get_option('ga-code');

    if($gaCode) {
        echo stripslashes($gaCode);
    }
    ?>

    <?php
    $locale = get_locale();
    $locale = explode('_', $locale);
    $locale = $locale[0];
    ?>

    <script>
        (function() {
            jQuery.getJSON('http://ipinfo.io/json', function(response) {
                var path = window.location.pathname,
                    pathArray = path.split('/'),
                    newPath, newLang,
                    lang = '<?php echo $locale; ?>';

                if(lang) {
                    switch(response.country) {
                        case 'PL':
                            newPath = '/';
                            newLang = 'pl';
                            break;
                        case 'EN' || 'US':
                            newPath = 'en';
                            newLang = 'en';
                            break;
                        case 'DE':
                            newPath = 'de';
                            newLang = 'de';
                            break;
                        case 'RU' || 'UA':
                            newPath = 'ru';
                            newLang = 'ru';
                            break;
                    }

                    if(!localStorage.getItem('redirected_to_lang_canceled') && newLang && !localStorage.getItem('redirected_to_lang') && newLang !== lang) {
                        var redirect = confirm('Dear User! We have detected that You are using wrong language version. Do You want to see Our website in Your language?');

                        if(redirect) {
                            localStorage.setItem('redirected_to_lang', true);
                            window.location.href = '//' + window.location.host + '/' + newPath;
                        } else {
                            localStorage.setItem('redirected_to_lang', false);
                        }
                    } else if(newLang && localStorage.getItem('redirected_to_lang') === 'true' && newLang !== lang) {
                        window.location.href = '//' + window.location.host + '/' + newPath;
                    }
                }
            });

            jQuery(document).ready(function() {
                jQuery('.lang-menu a').on('click', function($e) {
                    $e.preventDefault();

                    localStorage.setItem('redirected_to_lang_canceled', true);
                    window.location.href = jQuery($e.target).attr('href');
                });
            });
        }());

    </script>
</head>

<body <?php if(is_home()) { echo 'class="home"'; }; ?> ng-app="app" app-directive>

<header id="main-header" header-directive>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="col-md-4 col-sm-5 col-xs-10">
                <h1>
                    <a href="/" class="logo" title="<?php echo get_bloginfo('name'); ?>">
                        <img src="<?php echo get_option('page-logo'); ?>" alt="<?php echo get_bloginfo('name'); ?>"
                             class="img-responsive">
                    </a>
                </h1>
            </div>
            <div class="col-md-offset-1 col-md-7 col-sm-7 col-sm-offset-0 col-xs-2">
                <div class="menu visible-md visible-lg">
                    <?php wp_nav_menu(array(
                        'menu' => 'main-menu',
                        'menu_class' => '',
                        'container' => 'nav',
                        'container_class' => 'main-menu'
                    )); ?>

                    <?php wp_nav_menu(array(
                        'menu' => 'lang-menu',
                        'menu_class' => '',
                        'container' => 'nav',
                        'container_class' => 'lang-menu'
                    )); ?>
                </div>

                <div class="visible-xs visible-sm">
                    <menu-button-component></menu-button-component>
                </div>
            </div>
        </div>
    </div>
</header>
