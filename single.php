<?php
get_header();
$blogPage = get_page_by_path(BLOG_URL);
$blogId = $blogPage->ID;
?>

    <div class="main-container">
        <section class="section section-subpage-banner" style="background-image: url(<?php the_field('banner', $blogId); ?>);">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="center text-center">
                            <article>
                                <h2><?php the_field('banner_title', $blogId); ?></h2>
                                <p><?php the_field('banner_description', $blogId); ?></p>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section blog-single">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-content">
                                <div class="news-image">
                                    <img src="<?php the_field('news_image'); ?>" alt="<?php the_title(); ?>" class="img-responsive">
                                </div>

                                <div class="return-to-list">
                                    <a href="<?php echo BLOG_URL; ?>" title="<?php _e('Wróć do listy wpisów >', 'btw'); ?>"><?php _e('Wróć do listy wpisów >', 'btw'); ?></a>
                                </div>

                                <?php
                                if ( have_posts() ) {
                                    while (have_posts()) {
                                        the_post();
                                        ?>
                                        <article>
                                            <h2><?php the_title(); ?></h2>
                                            <span class="date"><?php echo get_the_date(); ?></span>
                                            <div class="lead"><?php the_excerpt(); ?></div>
                                            <?php the_content(); ?>
                                        </article>
                                        <?php
                                    }
                                }
                                ?>

                                <div class="prev-next">
                                    <?php
                                    $prev_post = get_previous_post();
                                    $next_post = get_next_post();
                                    ?>

                                    <?php if($prev_post): ?>
                                        <a href="<?php echo get_permalink($prev_post->ID); ?>" title="<?php echo $prev_post->post_title; ?>" class="prev">
                                            <i class="icon icon-arrow-bold-left"></i> <?php _e('Starszy', 'btw'); ?>
                                        </a>
                                    <?php
                                        endif;
                                        if($next_post):
                                    ?>
                                        <a href="<?php echo get_permalink($next_post->ID); ?>" title="<?php echo $next_post->post_title; ?>" class="next">
                                            <?php _e('Nowszy', 'btw'); ?> <i class="icon icon-arrow-bold-left"></i>
                                        </a>
                                    <?php
                                        endif;
                                    ?>
                                </div>

                                <div class="last-posts">
                                    <?php
                                    $args = array(
                                        'post_type' => 'post',
                                        'post__not_in' => array($post->ID),
                                        'posts_per_page' => 5,
                                        'order'     => 'DESC'
                                    );

                                    $query = new WP_Query($args);
                                    ?>
                                    <ul>
                                        <?php
                                        if ( $query->have_posts() ) {
                                            while ($query->have_posts()) {
                                                $query->the_post();
                                                ?>
                                                <li>
                                                    <a href="<?php echo get_permalink($post->ID); ?>" title="<?php the_title(); ?>" class="item">
                                                        <div class="image">
                                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="img-responsive">
                                                        </div>
                                                        <h4><?php the_title(); ?></h4>
                                                        <span class="date"><?php echo get_the_date(); ?></span>
                                                    </a>
                                                </li>
                                            <?php
                                                }
                                            }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>

        <?php wp_reset_query(); ?>

        <div class="breadcrumbs">
            <?php
            $url = get_permalink();
            $urlArr = parse_url($url);
            $pathArr = explode('/', $urlArr['path']);
            array_shift ($pathArr);
            array_pop($pathArr);
            $lastElementUrl = array_pop($pathArr);
            $pageUrl = '';
            ?>
            <div class="container">
                <div class="col-md-12">
                    <a href="<?php echo site_url(); ?>" title="Walusiak">Walusiak</a>
                    <?php foreach($pathArr as $link):
                        $pageUrl .= '/' . $link;
                        $pageObj = get_page_by_path($pageUrl);
                        ?>
                        <a href="<?php echo $pageUrl; ?>" title="<?php echo $pageObj->post_title; ?>"><?php echo $pageObj->post_title; ?></a>
                        <?php
                    endforeach;
                    $lastElementUrl = $pageUrl . '/' . $lastElementUrl;
                    $lastElementObj = get_page_by_path('/' . $lastElementUrl);
                    ?>

                    <span><?php the_title(); ?></span>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>