<?php
get_header();
?>

    <div class="main-container">
        <section class="section section-subpage-banner" style="background-image: url(<?php the_field('banner'); ?>);">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="center text-center">
                            <article>
                                <h2><?php the_field('banner_title'); ?></h2>
                                <p><?php the_field('banner_description'); ?></p>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section default-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-content">
                                <article>
                                <?php
                                if ( have_posts() ) {
                                    while (have_posts()) {
                                        the_post();

                                        the_content();
                                    };
                                }
                                ?>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
        </section>

        <?php wp_reset_query(); ?>

        <div class="breadcrumbs">
            <?php
            $url = get_permalink();
            $urlArr = parse_url($url);
            $pathArr = explode('/', $urlArr['path']);
            $pageUrl = '';
            ?>
            <div class="container">
                <div class="col-md-12">
                    <a href="<?php site_url(); ?>" title="Walusiak">Walusiak</a>
                    <?php foreach($pathArr as $link):
                        $pageUrl .= '/' . $link;
                        $pageObj = get_page_by_path($pageUrl);
                        if($pageObj && $link):
                            ?>
                            <a href="<?php echo $pageUrl; ?>" title="<?php echo $pageObj->post_title; ?>"><?php echo $pageObj->post_title; ?></a>
                            <?php
                        endif;
                    endforeach;

                    if(is_single()) {
                        ?>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>