<?php
/*----------
Template name: Budowa maszyn
---------- */

get_header();
?>

    <div class="main-container">
        <section class="section section-subpage-banner" style="background-image: url(<?php the_field('banner'); ?>);">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="center text-center">
                            <article>
                                <h2><?php the_field('banner_title'); ?></h2>
                                <p><?php the_field('banner_description'); ?></p>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php
        if( have_rows('sliders') && get_field('sliders_visible') ):
            while( have_rows('sliders') ) : the_row();
        ?>
        <section class="section section-image-slider">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-content">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <images-slider-component images='<?php echo json_encode(get_sub_field('images')); ?>'></images-slider-component>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <article>
                                        <h3><?php the_sub_field('title'); ?></h3>
                                        <?php the_sub_field('content'); ?>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
            endwhile;
        endif;
        ?>

        <?php if(get_field('banner_visible')): ?>
            <section class="section section-banner" style="background-image: url('<?php the_field("banner_image"); ?>')">
            </section>
        <?php endif; ?>

        <?php if(get_field('items_carousel_visible')): ?>
        <section class="section section-carousel">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-content">
                            <h3><?php the_field('items_carousel_title'); ?></h3>
                            <carousel-component items='<?php echo json_encode(get_field('items_carousel')); ?>'></carousel-component>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>

        <?php if(get_field('youtube_url') && get_field('video_visible')): ?>
        <section class="section section-video">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-content">
                            <h3><?php the_field('video_title'); ?></h3>
                            <article><?php the_field('video_description'); ?></article>
                            <div class="movie">
                                <iframe src="<?php the_field('youtube_url'); ?>">
                                </iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>

        <div class="breadcrumbs">
            <?php
            $url = get_permalink();
            $urlArr = parse_url($url);
            $pathArr = explode('/', $urlArr['path']);
            array_shift ($pathArr);
            array_pop($pathArr);
            $lastElementUrl = array_pop($pathArr);
            $pageUrl = '';
            ?>
            <div class="container">
                <div class="col-md-12">
                    <a href="<?php echo site_url(); ?>" title="Walusiak">Walusiak</a>
                    <?php foreach($pathArr as $link):
                        $pageUrl .= '/' . $link;
                        $pageObj = get_page_by_path($pageUrl);
                        ?>
                        <a href="<?php echo $pageUrl; ?>" title="<?php echo $pageObj->post_title; ?>"><?php echo $pageObj->post_title; ?></a>
                        <?php
                    endforeach;
                    $lastElementUrl = $pageUrl . '/' . $lastElementUrl;
                    $lastElementObj = get_page_by_path('/' . $lastElementUrl);
                    ?>

                    <span><?php echo $lastElementObj->post_title; ?></span>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>