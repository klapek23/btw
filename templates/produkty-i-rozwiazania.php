<?php
/*----------
Template name: Produkty i rozwiazania
---------- */

get_header();
?>

    <div class="main-container">
        <section class="section section-subpage-banner" style="background-image: url(<?php the_field('banner'); ?>);">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="center text-center">
                            <article>
                                <h2><?php the_field('banner_title'); ?></h2>
                                <p><?php the_field('banner_description'); ?></p>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-product-list section-product-list-3 section-product-list-inverted section-products-and-services-hidden section-blue">
            <div class="section-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title">
                                <div><em></em></div>
                                <div>
                                    <h2><?php the_field('products_title'); ?></h2>
                                </div>
                                <div><em></em></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                        $i = 1;
                        if( have_rows('products_items') ):
                            while( have_rows('products_items') ) : the_row();
                                ?>
                                <div class="col-md-4">
                                    <a href="<?php the_sub_field('link'); ?>" title="<?php the_sub_field('name'); ?>" class="item <?php echo ($i < 4 ? 'visible' : ''); ?>">
                                        <article>
                                            <h3><?php the_sub_field('title'); ?></h3>
                                        </article>
                                        <div class="image">
                                            <img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('name'); ?>">
                                        </div>
                                    </a>
                                </div>

                                <?php if($i % 3 == 0): ?>
                                <div class="clearfix"></div>
                                <?php endif;
                                $i++;
                            endwhile;
                        endif;
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="load-more">
                                <button load-more-directive type="button" items-count='<?php echo json_encode(count(get_field('products_items'))); ?>' class="button-primary"><?php _e('Zobacz więcej', 'btw'); ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-product-list section-product-list-3 section-solutions">
            <div class="section-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title">
                                <div><em></em></div>
                                <div>
                                    <h2><?php the_field('solutions_title'); ?></h2>
                                </div>
                                <div><em></em></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                        if( have_rows('solutions_items') ):
                            while( have_rows('solutions_items') ) : the_row();
                                ?>

                                <div class="col-md-4">
                                    <a href="<?php the_sub_field('link'); ?>" title="<?php the_sub_field('name'); ?>" class="item">
                                        <div class="image">
                                            <img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('name'); ?>">
                                        </div>
                                        <article>
                                            <h3><?php the_sub_field('title'); ?></h3>
                                        </article>
                                    </a>
                                </div>

                                <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </section>

        <div class="breadcrumbs">
            <?php
            $url = get_permalink();
            $urlArr = parse_url($url);
            $pathArr = explode('/', $urlArr['path']);
            array_shift ($pathArr);
            array_pop($pathArr);
            $lastElementUrl = array_pop($pathArr);
            $pageUrl = '';
            ?>
            <div class="container">
                <div class="col-md-12">
                    <a href="<?php echo site_url(); ?>" title="Walusiak">Walusiak</a>
                    <?php foreach($pathArr as $link):
                        $pageUrl .= '/' . $link;
                        $pageObj = get_page_by_path($pageUrl);
                        ?>
                        <a href="<?php echo $pageUrl; ?>" title="<?php echo $pageObj->post_title; ?>"><?php echo $pageObj->post_title; ?></a>
                        <?php
                    endforeach;
                    $lastElementUrl = $pageUrl . '/' . $lastElementUrl;
                    $lastElementObj = get_page_by_path('/' . $lastElementUrl);
                    ?>

                    <span><?php echo $lastElementObj->post_title; ?></span>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>