<?php
/*----------
Template name: Mission
---------- */

get_header(); ?>

    <div class="main-container">
        <section class="section section-subpage-banner" style="background-image: url(<?php the_field('banner'); ?>);">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="center text-center">
                            <article>
                                <h2><?php the_field('banner_title'); ?></h2>
                                <p><?php the_field('banner_description'); ?></p>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php
            $items = get_field('items');
        ?>
        <box-multiple-component items='<?php echo htmlspecialchars(json_encode($items)); ?>'></box-multiple-component>

        <div class="breadcrumbs">
            <?php
            $url = get_permalink();
            $urlArr = parse_url($url);
            $pathArr = explode('/', $urlArr['path']);
            array_shift ($pathArr);
            array_pop($pathArr);
            $lastElementUrl = array_pop($pathArr);
            $pageUrl = '';
            ?>
            <div class="container">
                <div class="col-md-12">
                    <a href="<?php echo site_url(); ?>" title="Walusiak">Walusiak</a>
                    <?php foreach($pathArr as $link):
                        $pageUrl .= '/' . $link;
                        $pageObj = get_page_by_path($pageUrl);
                        ?>
                        <a href="<?php echo $pageUrl; ?>" title="<?php echo $pageObj->post_title; ?>"><?php echo $pageObj->post_title; ?></a>
                        <?php
                    endforeach;
                    $lastElementUrl = $pageUrl . '/' . $lastElementUrl;
                    $lastElementObj = get_page_by_path('/' . $lastElementUrl);
                    ?>

                    <span><?php echo $lastElementObj->post_title; ?></span>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>