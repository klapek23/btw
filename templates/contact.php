<?php
/*----------
Template name: Contact
---------- */
get_header();
?>

    <div class="main-container">
        <section class="full-width-map">
            <div id="gmap"></div>

            <script type="text/javascript">
                var map,
                    lat = parseFloat("<?php the_field('map_center_latitude'); ?>"),
                    lng = parseFloat("<?php the_field('map_center_longitude'); ?>"),
                    address = <?php echo json_encode(get_field('address')); ?>,
                    locale = <?php echo json_encode($locale); ?>,
                    center = (locale !== 'pl_PL' ? { lat: lat + 2, lng: lng } : { lat: lat, lng: lng });

                function initMap() {
                    map = new google.maps.Map(document.getElementById('gmap'), {
                        center: center,
                        zoom: (locale !== 'pl_PL' ? 6 : 10),
                        draggable: !("ontouchend" in document)
                    });

                    var infoWindowContent = '<div class="gmap-infowindow">' +
                        '<p>' + address + '</p>' +
                        '</div>';

                    var infowindow = new google.maps.InfoWindow({
                        content: infoWindowContent
                    });

                    var marker = new google.maps.Marker({
                        position: {
                            lat: lat,
                            lng: lng
                        },
                        map: map,
                        title: address
                    });

                    marker.addListener('click', function() {
                        infowindow.open(map, marker);
                    });
                }
            </script>

            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsj44LC6-pdJBAnfy1vpwGxOknG3M1njs&callback=initMap"async defer></script>
        </section>

        <section class="section contact">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-content">
                            <div class="col-md-6">
                                <h2><?php the_field('form_title'); ?></h2>
                                <?php do_shortcode(get_field('contact_form_shortcode')); ?>
                            </div>
                            <div class="col-md-6">
                                <div class="address-list">
                                    <address>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <?php the_field('address_column_1'); ?>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <?php the_field('address_column_2'); ?>
                                            </div>
                                        </div>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php wp_reset_query(); ?>

        <div class="breadcrumbs">
            <?php
            $url = get_permalink();
            $urlArr = parse_url($url);
            $pathArr = explode('/', $urlArr['path']);
            array_shift ($pathArr);
            array_pop($pathArr);
            $lastElementUrl = array_pop($pathArr);
            $pageUrl = '';
            ?>
            <div class="container">
                <div class="col-md-12">
                    <a href="<?php echo site_url(); ?>" title="Walusiak">Walusiak</a>
                    <?php foreach($pathArr as $link):
                        $pageUrl .= '/' . $link;
                        $pageObj = get_page_by_path($pageUrl);
                        ?>
                        <a href="<?php echo $pageUrl; ?>" title="<?php echo $pageObj->post_title; ?>"><?php echo $pageObj->post_title; ?></a>
                        <?php
                    endforeach;
                    $lastElementUrl = $pageUrl . '/' . $lastElementUrl;
                    $lastElementObj = get_page_by_path('/' . $lastElementUrl);
                    ?>

                    <span><?php echo $lastElementObj->post_title; ?></span>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>