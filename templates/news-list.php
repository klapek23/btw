<?php
/*----------
Template name: News list
---------- */

get_header(); ?>

    <div class="main-container">
        <section class="section section-subpage-banner" style="background-image: url(<?php the_field('banner'); ?>);">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="center text-center">
                            <article>
                                <h2><?php the_field('banner_title'); ?></h2>
                                <p><?php the_field('banner_description'); ?></p>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section blog-list">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="section-content">
                            <?php
                                $per_page = 3;
                                $uri = $_SERVER['REQUEST_URI'];
                                $urlArr = parse_url($uri);
                                $pathArr = explode('/', $urlArr['path']);
                                array_pop($pathArr);

                                $currentPage = (is_numeric(end($pathArr)) ? intval(end($pathArr)) : 0);
                                $offset = (is_numeric(end($pathArr)) ? (intval(end($pathArr) - 1) * $per_page) : 0);
                                $args = array(
                                    'post_type' => 'post',
                                    'posts_per_page' => $per_page,
                                    'offset'    => $offset,
                                    'order'     => 'DESC'
                                );

                                $query = new WP_Query($args);

                                if ( $query->have_posts() ) {
                                    while ($query->have_posts()) {
                                        $query->the_post();
                                ?>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <a class="item" href="<?php echo get_permalink($post->ID); ?>" title="<?php the_title(); ?>">
                                                <div class="col-md-4">
                                                    <div class="image">
                                                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="img-responsive">
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <article>
                                                        <h3><?php the_title(); ?></h3>
                                                        <span class="date"><?php echo get_the_date(); ?></span>
                                                        <div class="excerpt"><?php the_excerpt(); ?></div>
                                                        <span class="more"><?php _e('Czytaj więcej', 'btw'); ?></span>
                                                    </article>
                                                </div>
                                            </a>
                                        </div>
                                    </div>

                                <?php
                                    }
                                } else {
                                    echo '<p>' . __('Nothing to display', 'btw') . '</p>';
                                }

                                wp_reset_query();

                                $allPosts = get_posts(array(
                                    'post_type' => 'post',
                                    'posts_per_page'  => -1
                                ));

                                $countPages = ceil(count($allPosts) / $per_page);

                                if($countPages): ?>
                                    <nav class="blog-pagination">
                                        <ul>
                                            <?php
                                                for($i = 0; $i < $countPages; $i++) {
                                                ?>
                                                <li>
                                                    <a href="<?php echo BLOG_URL . '?page=' . ($i+1); ?>" class="<?php echo
                                                    ($currentPage == $i + 1 ? 'active' : ''); ?> <?php echo
                                                    (!$currentPage && $i == 0 ? 'active' : ''); ?>"><?php echo $i+1;?></a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </nav>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
        </section>

        <div class="breadcrumbs">
            <?php
            $url = get_permalink();
            $urlArr = parse_url($url);
            $pathArr = explode('/', $urlArr['path']);
            array_shift ($pathArr);
            array_pop($pathArr);
            $lastElementUrl = array_pop($pathArr);
            $pageUrl = '';
            ?>
            <div class="container">
                <div class="col-md-12">
                    <a href="<?php echo site_url(); ?>" title="Walusiak">Walusiak</a>
                    <?php foreach($pathArr as $link):
                        $pageUrl .= '/' . $link;
                        $pageObj = get_page_by_path($pageUrl);
                        ?>
                        <a href="<?php echo $pageUrl; ?>" title="<?php echo $pageObj->post_title; ?>"><?php echo $pageObj->post_title; ?></a>
                        <?php
                    endforeach;
                    $lastElementUrl = $pageUrl . '/' . $lastElementUrl;
                    $lastElementObj = get_page_by_path('/' . $lastElementUrl);
                    ?>

                    <span><?php echo $lastElementObj->post_title; ?></span>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>