<?php
/*----------
Template name: Home
---------- */

get_header(); ?>

    <div class="main-container">
        <?php
            $sliderFullWidth = new stdClass();
            $sliderFullWidth->slides = get_field('section_slider_full_width');
        ?>
        <section-slider-full-width-component items='<?php echo json_encode($sliderFullWidth->slides);?>'></section-slider-full-width-component>

        <section class="section section-product-list section-product-list-3 section-products-and-services">
            <div class="section-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title">
                                <div><em></em></div>
                                <div>
                                    <h2><?php the_field('product_and_services_title'); ?></h2>
                                </div>
                                <div><em></em></div>
                            </div>
                            <div class="tags"><?php the_field('product_and_services_tags'); ?></div>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                        if( have_rows('product_and_services_items') ):
                            while( have_rows('product_and_services_items') ) : the_row();
                                ?>

                                <div class="col-md-3">
                                    <a href="<?php the_sub_field('link'); ?>" title="<?php the_sub_field('name'); ?>" class="item">
                                        <div class="icon">
                                            <span class="<?php the_sub_field('icon'); ?>"></span>
                                        </div>
                                        <article>
                                            <h3><?php the_sub_field('name'); ?></h3>
                                            <p><?php the_sub_field('description'); ?></p>
                                            <span><?php _e('Zobacz więcej', 'btw'); ?></span>
                                        </article>
                                    </a>
                                </div>

                                <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-banner with-link" style="background-image: url('<?php the_field
        ("banner_01_image"); ?>')">
            <a href="<?php the_field('banner_01_link'); ?>" title="<?php the_field('banner_01_text'); ?>">
                <div>
                    <h3><?php the_field('banner_01_text'); ?></h3>
                    <button type="button" class="button banner-button"><?php _e('Zobacz', 'btw'); ?></button>
                </div>
            </a>
        </section>

        <section class="section section-product-list section-product-list-4 section-services">
            <div class="section-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title">
                                <div><em></em></div>
                                <div>
                                    <h2><?php the_field('services_title'); ?></h2>
                                </div>
                                <div><em></em></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                        if( have_rows('services_items') ):
                            while( have_rows('services_items') ) : the_row();
                                ?>

                                <div class="col-md-3">
                                    <?php if(get_sub_field('link')) { ?>
                                        <a href="<?php the_sub_field('link'); ?>" title="<?php the_sub_field('name'); ?>" class="item">
                                    <?php } else { ?>
                                        <div class="item">
                                    <?php } ?>

                                        <div class="image">
                                            <img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('name'); ?>">
                                        </div>
                                        <article>
                                            <h3><?php the_sub_field('name'); ?></h3>
                                            <p><?php the_sub_field('description'); ?></p>
                                        </article>

                                    <?php if(get_sub_field('link')) { ?>
                                        </a>
                                    <?php } else { ?>
                                        </div>
                                    <?php } ?>
                                </div>

                                <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-banner" style="background-image: url('<?php the_field("banner_02_image"); ?>')">
        </section>

        <section class="section section-blue section-product-list section-product-list-3 section-about-us">
            <div class="section-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title">
                                <div><em></em></div>
                                <div>
                                    <h2><?php the_field('about_us_title'); ?></h2>
                                </div>
                                <div><em></em></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                        if( have_rows('about_us_items') ):
                            while( have_rows('about_us_items') ) : the_row();
                                ?>

                                <div class="col-md-4">
                                    <a href="<?php the_sub_field('link'); ?>" title="<?php the_sub_field('name'); ?>" class="item">
                                        <div class="image">
                                            <img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('name'); ?>">
                                        </div>
                                        <article>
                                            <h3><?php the_sub_field('name'); ?></h3>
                                            <p><?php the_sub_field('description'); ?></p>
                                            <span><?php _e('Zobacz więcej', 'btw'); ?></span>
                                        </article>
                                    </a>
                                </div>

                            <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </section>

    <section class="section section-contact">
        <div class="section-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title">
                            <div><em></em></div>
                            <div>
                                <h2><?php the_field('contact_title'); ?></h2>
                            </div>
                            <div><em></em></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div id="gmap"></div>

                        <script type="text/javascript">
                            var map,
                                lat = parseFloat("<?php the_field('map_center_latitude'); ?>"),
                                lng = parseFloat("<?php the_field('map_center_longitude'); ?>"),
                                address = <?php echo json_encode(get_field('address')); ?>;

                            function initMap() {
                                map = new google.maps.Map(document.getElementById('gmap'), {
                                    center: {lat: lat, lng: lng},
                                    zoom: 6,
                                    draggable: !("ontouchend" in document)
                                });

                                var infoWindowContent = '<div class="gmap-infowindow">' +
                                    '<p>' + address + '</p>' +
                                    '</div>';

                                var infowindow = new google.maps.InfoWindow({
                                    content: infoWindowContent
                                });

                                var marker = new google.maps.Marker({
                                    position: {
                                        lat: lat,
                                        lng: lng
                                    },
                                    map: map,
                                    title: address
                                });

                                marker.addListener('click', function() {
                                    infowindow.open(map, marker);
                                });
                            }
                        </script>

                        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsj44LC6-pdJBAnfy1vpwGxOknG3M1njs&callback=initMap"async defer></script>
                    </div>
                    <div class="col-md-6">
                        <div class="address-list">
                            <address>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <?php the_field('contact_column_1'); ?>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <?php the_field('contact_column_2'); ?>
                                    </div>
                                </div>
                            </address>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>

<?php get_footer(); ?>