<?php get_header(); ?>

    <div class="main-container not-found">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2><strong><?php _e('Błąd 404:', 'klapek23_framework'); ?></strong> <?php _e('strony nie odnaleziono!', 'btw'); ?></h2>
                    <p><?php _e('Odśwież zawartosć lub przejdź do', 'btw'); ?> <a href="<?php site_url(); ?>"><?php _e('strony głównej', 'btw'); ?></a>.</p>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>