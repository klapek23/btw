<?php
/*----------
Template name: Single product
---------- */

get_header();

$productsPerPage = -1;
$productsOffset = 0;
$productsQueryArgs = array(
    'post_type' => 'products',
    'posts_per_page' => $productsPerPage,
    'offset'    => $offset,
    'order'     => 'DESC',
    //'post__not_in' => array($post->ID)
);

$productsQuery = new WP_Query($productsQueryArgs);
$allProducts = $productsQuery->get_posts();

foreach($allProducts as $key => $product) {
    $allProducts[$key]->link = get_permalink($product->ID);
    $allProducts[$key]->image = new stdClass();
    $allProducts[$key]->image->url = get_the_post_thumbnail_url($product->ID);
}
?>

        <product-carousel-component items='<?php echo json_encode($allProducts); ?>' open-text="<?php _e('Rozwiń listę produktów', 'btw'); ?>" close-text="<?php _e('Zwiń listę produktów', 'btw'); ?>"></product-carousel-component>

        <section class="section section-banner section-banner-only-title">
            <h3><?php the_field('product_name'); ?></h3>
        </section>

        <section class="section section-image-slider section-product-info">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-content">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <?php
                                    $productImages = get_field('product_info_images');
                                    foreach($productImages as $key => $image):
                                        $productImages[$key]['image'] = $image['image']['url'];
                                    endforeach;
                                    ?>
                                    <images-slider-component images='<?php echo json_encode($productImages); ?>'></images-slider-component>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <article>
                                        <?php the_field('product_info_text'); ?>
                                    </article>
                                    <?php if(get_field('product_info_specification_link')): ?>
                                    <a href="<?php the_field('product_info_specification_link'); ?>" title="<?php _e('Specyfikacja', 'btw'); ?>" class="button button-primary"><?php _e('Specyfikacja', 'btw'); ?></a>
                                    <?php endif; ?>

                                    <?php if(get_field('product_info_folder_link')): ?>
                                    <a href="<?php the_field('product_info_folder_link'); ?>" title="<?php _e('Folder', 'btw'); ?>" class="button button-primary"><?php _e('Folder', 'btw'); ?></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php if(get_field('specification_table_visible')): ?>
        <section class="section section-product-specification">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2><?php _e('Specyfikacja', 'btw'); ?></h2>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 col-sm-12 col-sm-offset-0">
                                <table>
                                    <thead>
                                        <tr>
                                            <th><?php _e('Dane techniczne', 'btw'); ?></th>
                                            <th><?php _e('Wartość', 'btw'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if( have_rows('specification_table') ):
                                            while( have_rows('specification_table') ) : the_row();
                                                ?>

                                                <tr>
                                                    <td><?php the_sub_field('key'); ?></td>
                                                    <td><?php the_sub_field('value'); ?></td>
                                                </tr>

                                                <?php
                                            endwhile;
                                        endif;
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>

        <?php if(get_field('banner_image_visible')): ?>
        <section class="section section-banner" style="background-image: url('<?php the_field("banner_image"); ?>')">
        </section>
        <?php endif; ?>

        <?php if(get_field('product_additional_info_visible')): ?>
        <section class="section section-product-additional-info">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-md-offset-1 col-sm-12 col-sm-offset-0">
                        <div class="section-content">
                            <h3><?php the_field('product_additional_info_title'); ?></h3>
                            <article>
                                <?php the_field('product_additional_info_text'); ?>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>

        <?php if(get_field('special_features_visible')): ?>
        <section class="section section-carousel">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-content">
                            <h3><?php the_field('special_features_title'); ?></h3>
                            <carousel-component items='<?php echo json_encode(get_field('special_features_slider')); ?>'></carousel-component>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>

        <?php if(get_field('video_visible')): ?>
        <section class="section section-video">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-content">
                            <h3><?php the_field('video_title'); ?></h3>
                            <article><?php the_field('video_description'); ?></article>
                            <div class="movie">
                                <iframe src="<?php the_field('youtube_url'); ?>">
                                </iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>

        <div class="breadcrumbs">
            <?php
            $url = get_permalink();
            $urlArr = parse_url($url);
            $pathArr = explode('/', $urlArr['path']);
            array_pop($pathArr);
            $lastElementUrl = array_pop($pathArr);
            $lastElementObj = get_page_by_path($lastElement);
            $pageUrl = '';
            ?>
            <div class="container">
              <div class="col-md-12">
                <a href="<?php echo site_url(); ?>" title="Walusiak">Walusiak</a>
                <?php
                if(is_single()) {
                  $productsPageUrl = get_option('products-page-url');
                  $productsPageObj = get_page_by_path($productsPageUrl);
                  ?>
                  <a href="<?php echo get_permalink($productsPageObj->ID); ?>" title="<?php echo $productsPageObj->post_title; ?>"><?php echo $productsPageObj->post_title; ?></a>
                  <span><?php the_title(); ?></span>
                  <?php
                }
                ?>
              </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>