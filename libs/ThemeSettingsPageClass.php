<?php
//theme settings page class

class ThemeSettingsPageClass {

    //add page to menu
    public function addPageToMenu() {
        add_submenu_page('themes.php', 'Theme settings page', 'Theme settings', 'manage_options', 'theme-settings', array($this, 'theme_settings'));
    }

    //print page
    public function theme_settings() {
        $this->theme_settings_scripts();
        $this->theme_settings_styles();
        $this->print_content();
    }

    //update fields in database
    private function themeoptions_update() {
        update_option('page-logo', $_POST['page-logo']);
        update_option('products-page-url', $_POST['products-page-url']);
        update_option('footer-logo', $_POST['footer-logo']);
        update_option('footer-logo-text', $_POST['footer-logo-text']);
        update_option('footer-contact', $_POST['footer-contact']);
        update_option('ga-code', $_POST['ga-code']);
        update_option('fb-script-code', $_POST['fb-script-code']);
        update_option('cookies', $_POST['cookies']);
    }

    //add scripts to media uploader
    private function theme_settings_scripts() {
        wp_enqueue_script('jquery');
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_register_script('my-upload', get_template_directory_uri() . '/public_admin/my-upload.js',
            array('jquery','media-upload','thickbox'));
        wp_enqueue_script('my-upload');
    }

    private function theme_settings_styles() {
        wp_enqueue_style('thickbox');
        wp_register_style('custom-admin', get_template_directory_uri() . '/public_admin/custom-admin.css');
        wp_enqueue_style('custom-admin');
    }

    //print content
    private function print_content() { ?>
        <?php if ( isset($_POST['update_options']) && $_POST['update_options'] == 'true' ) { $this->themeoptions_update(); } ?>
        <div class="wrap">
            <div id="icon-themes" class="icon32"><br /></div>
            <h2><?php echo __('Theme settings', 'btw'); ?></h2>

            <form method="POST" action="">
                <ul>
                    <li class="optionContainer">
                        <h3><?php echo __('Header', 'btw'); ?></h3>
                        <div>
                            <label for="page-logo"><?php echo __('Page logo image:', 'btw'); ?></label>
                            <img src="<?php echo get_option('page-logo'); ?>" title="Page logo" style="display:
                            block; margin-bottom: 10px;">
                            <input type="text" name="page-logo" id="page-logo" value="<?php echo get_option('page-logo'); ?>" >
                            <button type="button" class="upload-button"  name="page-logo" id="page-logo-button" ><?php echo __('Choose', 'btw'); ?></button>
                        </div>
                    </li>
                    <li class="optionContainer">
                        <h3><?php echo __('Products page', 'btw'); ?>:</h3>
                        <div>
                            <label for="products-page-url" style="display: block;"><?php echo __('Products page URL', 'btw'); ?></label>
                            <input type="text" name="products-page-url" id="products-page-url" value="<?php echo stripslashes(get_option('products-page-url')); ?>">
                        </div>
                    </li>
                    <li class="optionContainer">
                        <h3><?php echo __('Footer', 'btw'); ?></h3>
                        <div>
                            <label for="footer-logo"><?php echo __('Footer logo image:', 'btw'); ?></label>
                            <img src="<?php echo get_option('footer-logo'); ?>" title="Footer logo" style="display:
                            block; margin-bottom: 10px;">
                            <input type="text" name="footer-logo" id="footer-logo" value="<?php echo get_option('footer-logo'); ?>" >
                            <button type="button" class="upload-button"  name="footer-logo" id="footer-logo-button" ><?php echo __('Choose', 'btw'); ?></button>
                        </div>

                        <h3><?php echo __('Foter logo text', 'btw'); ?>:</h3>
                        <div>
                            <label for="footer-logo-text" style="display: block;"><?php echo __('Footer logo text', 'btw'); ?></label>
                            <textarea name="footer-logo-text" id="footer-logo-text" rows="10" cols="60" ><?php echo stripslashes(get_option('footer-logo-text')); ?></textarea>
                        </div>

                        <h3><?php echo __('Foter contact', 'btw'); ?>:</h3>
                        <div>
                            <label for="footer-contact" style="display: block;"><?php echo __('Footer contact', 'btw'); ?></label>
                            <textarea name="footer-contact" id="footer-contact" rows="10" cols="60" ><?php echo stripslashes(get_option('footer-contact')); ?></textarea>
                        </div>
                    </li>
                    <li class="optionContainer">
                        <h3><?php echo __('Cookies box', 'btw'); ?>:</h3>
                        <div>
                            <label for="cookies" style="display: block;"><?php echo __('Cookies text', 'btw'); ?></label>
                            <textarea name="cookies" id="cookies" rows="10" cols="60" ><?php echo stripslashes(get_option('cookies')); ?></textarea>
                        </div>
                    </li>
                    <li class="optionContainer">
                        <h3><?php echo __('Google Analytics code', 'btw'); ?>:</h3>
                        <div>
                            <label for="ga-code" style="display: block;"><?php echo __('GA Code',
                                    'btw'); ?></label>
                            <textarea name="ga-code" id="ga-code" rows="10" cols="60" ><?php echo stripslashes(get_option('ga-code')); ?></textarea>
                        </div>
                    </li>
                    <li class="optionContainer">
                        <h3><?php echo __('Facebook Script code', 'btw'); ?>:</h3>
                        <div>
                            <label for="fb-script-code" style="display: block;"><?php echo __('Facebook Script code', 'btw'); ?></label>
                            <textarea name="fb-script-code" id="fb-script-code" rows="10" cols="60" ><?php echo stripslashes(get_option('fb-script-code')); ?></textarea>
                        </div>
                    </li>
                    <li>
                        <input type="hidden" name="update_options" value="true" />
                        <button type="submit" class="button"><?php echo __('Save', 'btw'); ?></button>
                    </li>
                </ul>
            </form>
        </div>
    <?php 
    }
}

?>