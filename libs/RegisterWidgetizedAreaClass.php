<?php
//register widgetized area

class RegisterWidgetizedAreaClass {

    public function widgets_init() {
        register_sidebar( array(
            'name'          => __( 'Footer text', 'btw' ),
            'id'            => 'footer-text',
            'before_title'  => '<h3>',
            'after_title'  => '</h3>',
            'before_widget' => '',
            'after_widget' => ''
        ) );
    }
    
}

?>