<footer id="main-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="footer-logo">
                    <img src="<?php echo get_option('footer-logo'); ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    <p><?php echo get_option('footer-logo-text'); ?></p>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-2 col-sm-6 col-sm-offset-0 col-xs-6">
                <div class="footer-menu">
                    <h3><?php _e('Menu', 'btw'); ?></h3>
                    <?php wp_nav_menu(array(
                        'menu' => 'main-menu',
                        'menu_class' => '',
                        'container' => 'nav',
                        'container_class' => 'main-menu-footer'
                    )); ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer-contact">
                    <h3><?php _e('Kontakt', 'btw'); ?></h3>
                    <p><?php echo get_option('footer-contact'); ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="footer-bottom">
                    <p>Copyright <strong>2005-<?php echo date('Y'); ?> WALUSIAK</strong></p>
                </div>
            </div>
        </div>
    </div>

    <?php wp_footer(); ?>

</footer>

</div>

<cookies-component><?php echo get_option('cookies'); ?></cookies-component>

<div menu-directive>
    <div class="responsive-menu visible-xs visible-sm" ng-if="$ctrl.menuService.isOpened">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="col-md-12">
                    <?php wp_nav_menu(array(
                        'menu' => 'main-menu',
                        'menu_class' => '',
                        'container' => 'nav',
                        'container_class' => 'main-menu-rwd'
                    )); ?>

                    <?php wp_nav_menu(array(
                        'menu' => 'lang-menu',
                        'menu_class' => '',
                        'container' => 'nav',
                        'container_class' => 'lang-menu'
                    )); ?>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>